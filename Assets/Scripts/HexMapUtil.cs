using System.Collections.Generic;
using UnityEngine;
using Zen.Hexagons;

namespace DefaultNamespace
{
    public static class HexMapUtil
    {
        public static HexMapHolder Generate(HexTile tilePrefab, int size)
        {
            HexTile[][] result = new HexTile[size][];
            for (int i = 0; i < size; i++)
            {
                result[i] = new HexTile[size];
                for (int j = 0; j < size; j++)
                {
                    result[i][j] = null;
                }
            }

            HexTile tempTile = Object.Instantiate(tilePrefab);
            float tileSize = tempTile.GetComponent<MeshCollider>().bounds.extents.x;
            Object.DestroyImmediate(tempTile.gameObject);
            
            HexLibrary hexLibrary = new HexLibrary(HexType.FlatTopped, OffsetCoordinatesType.Odd, tileSize);
            GameObject mapHolder = new GameObject("MapHolder");
            mapHolder.transform.position = Vector3.zero;
            HexMapHolder holder = mapHolder.AddComponent<HexMapHolder>();
            
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    HexOffsetCoordinates offsetCoordinates = new HexOffsetCoordinates(col, row);
                    Point2F pixelCoordinates = hexLibrary.FromOffsetCoordinatesToPixel(offsetCoordinates);
                    Vector3 hexCenterPos = new Vector3(pixelCoordinates.X, 0, pixelCoordinates.Y);
                    
                    HexTile newTile = Object.Instantiate(tilePrefab, hexCenterPos, tilePrefab.transform.rotation);
                    newTile.transform.parent = mapHolder.transform;
                    newTile.name = $"Tile_Row{row}_Col{col}";
                    newTile.row = row;
                    newTile.col = col;
                    
                    result[row][col] = newTile;
                }
            }

            holder.SetMap(result, hexLibrary);

            return holder;
        }
    }
    
    /*
     * var hexLibrary = new Zen.Hexagons.HexLibrary(HexType.FlatTopped, OffsetCoordinatesType.Odd, 64.0f);

var hexCube = hexLibrary.OffsetCoordinatesToCube(new HexOffsetCoordinates(2, 2));
var hexAxial = hexLibrary.OffsetCoordinatesToAxial(new HexOffsetCoordinates(2, 2));

var neighbors = hexLibrary.GetAllNeighbors(new HexOffsetCoordinates(1, 1));
var neighborNW = hexLibrary.GetNeighbor(new HexOffsetCoordinates(1, 1), Direction.NorthWest);

var ring1 = hexLibrary.GetSingleRing(new HexOffsetCoordinates(1, 1), 1);
var ring2 = hexLibrary.GetSingleRing(new HexOffsetCoordinates(2, 2), 2);

var ring3 = hexLibrary.GetSpiralRing(new HexOffsetCoordinates(2, 2), 2);

var line = hexLibrary.GetLine(new HexOffsetCoordinates(1, 0), new HexOffsetCoordinates(3, 4));

var distance = hexLibrary.GetDistance(new HexOffsetCoordinates(1, 0), new HexOffsetCoordinates(3, 4));

var pixel = hexLibrary.FromOffsetCoordinatesToPixel(new HexOffsetCoordinates(1, 1));

var coords = hexLibrary.FromPixelToOffsetCoordinates(96, 166);
     */
}