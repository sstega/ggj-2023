using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [InitializeOnLoad]
    public static class EditorHotkeysTracker
    {
        static EditorHotkeysTracker()
        {
            SceneView.duringSceneGui += view =>
            {
                var e = Event.current;
                if (e != null && e.type == EventType.KeyUp)
                {
                    switch (e.keyCode)
                    {
                        case KeyCode.U:
                            SetTiles(TerrainType.Dirt);
                            break;
                        case KeyCode.I:
                            SetTiles(TerrainType.Rock);
                            break;
                        case KeyCode.O:
                            SetTiles(TerrainType.Compost);
                            break;
                    }
                }
            };
        }

        private static void SetTiles(TerrainType terrainType)
        {
            if (Selection.gameObjects == null)
            {
                return;
            }

            foreach (GameObject obj in Selection.gameObjects)
            {
                HexTile tile = obj.GetComponentInParent<HexTile>();
                if (tile != null)
                {
                    tile.TerrainType = terrainType;
                }
                EditorUtility.SetDirty(tile);
            }
            
        }
    }
}