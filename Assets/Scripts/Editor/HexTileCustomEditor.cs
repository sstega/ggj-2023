using System;
using DefaultNamespace;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(HexTile))]
    [CanEditMultipleObjects]
    public class HexTileCustomEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Refresh view"))
            {
                foreach (var obj in targets)
                {
                    ((HexTile)obj).RefreshView();
                }
            }
        }

        private void OnSceneGUI()
        {
            var currentEvent = Event.current;
            if (currentEvent.isKey && currentEvent.type == EventType.KeyUp)
            {
                switch (currentEvent.keyCode)
                {
                    case KeyCode.U:
                        Debug.Log($"U clicked on {target.name}");
                        break;
                }
            }
        }
    }
}