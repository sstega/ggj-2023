using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEditor;
using UnityEngine;

public class MapEditorWindow : EditorWindow
{
    [MenuItem("Tools/Map Editor")]
    public static void Open()
    {
        MapEditorWindow mapEditorWindow = GetWindow<MapEditorWindow>();
        mapEditorWindow.Show();
    }

    [SerializeField]
    private int size = 5;

    private HexTile tileMapPrefab;
    private void OnEnable()
    {
        tileMapPrefab = AssetDatabase.LoadAssetAtPath<HexTile>("Assets/Prefabs/HexTile.prefab");
    }

    private void OnGUI()
    {
        size = (int) InputField("Width", size, GUILayout.Width(50));

        if (GUILayout.Button("Generate blank", GUILayout.Width(200)))
        {
            HexMapHolder map = HexMapUtil.Generate(tileMapPrefab, size);
            Transform mainCameraTransform = Camera.main.transform;
            Vector3 camPos = mainCameraTransform.position;
            camPos.x = map.GetCenterTilePosition().x;
            mainCameraTransform.position = camPos;
        }

        ProcessHotkeyInput();
    }

    private void ProcessHotkeyInput()
    {
        // var currentEvent = Event.current;
        // if (currentEvent.isKey && currentEvent.type == EventType.KeyUp)
        // {
        //     switch (currentEvent.keyCode)
        //     {
        //         case KeyCode.U:
        //             Debug.Log("U clicked");
        //             break;
        //     }
        // }
    }

    private static object InputField<T>(string label, T value, params GUILayoutOption[] options)
    {
        object result;
        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Label(label);
            GUILayout.Label(" ");
            switch (value)
            {
                case int i:
                    result =  EditorGUILayout.IntField(i, options);
                    break;
                case float f:
                    result = EditorGUILayout.FloatField(f, options);
                    break;
                case string s:
                    result = EditorGUILayout.TextField(s, options);
                    break;
                default:
                    throw new Exception($"Unsupported type {typeof(T)}");
            }
            GUILayout.FlexibleSpace();
        }
        return result;
    }
}
