using DefaultNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RulesAndEffects
{
	protected Plant plant;

	public RulesAndEffects(Plant plant )
	{
		this.plant = plant;
	}

	public virtual bool CanBePlanted(HexTile tile)
	{
		return true;
	}

	public virtual void OnPlant()
	{
		if (plant.GrowingOn.TerrainType == TerrainType.Compost)
		{
			GameController.I.ApplyCompostBonus(plant.GrowingOn);
		}
	}

	public virtual void OnDeath()
	{
		plant.GrowingOn.Level++;
		plant.GrowingOn.ClearPlant();
	}

	public virtual void OnOwnerTurnStart()
	{
		
	}
}

public class BushRAE : RulesAndEffects
{
	public BushRAE(Plant plant): base(plant) {}

	public override bool CanBePlanted(HexTile tile)
	{
		int numFriendlies = 0;
		foreach (HexTile n in tile.Neighbours)
		{
			if (n.Owner == GameController.I.currentPlayer)
			{
				numFriendlies++;
			}
			if (GameController.I.currentPlayer.HomeTile == n)
			{
				numFriendlies = 2;
			}
		}

		return numFriendlies >= 2;
	}
}

public class FlowerRAE : RulesAndEffects
{
	public FlowerRAE(Plant plant) : base(plant) { }
	public override bool CanBePlanted(HexTile tile)
	{
		int numFlowersAround = 0;
		foreach (HexTile n in tile.Neighbours)
		{
			if (n.Owner == GameController.I.currentPlayer && n.Plant?.DisplayName == "Flower")
			{
				numFlowersAround++;
			}
		}

		return numFlowersAround <= 2;
	}
}

public class MushroomRAE : RulesAndEffects
{
	public MushroomRAE(Plant plant) : base(plant) { }
	public override void OnDeath()
	{
		plant.GrowingOn.TerrainType = TerrainType.Dirt;
		base.OnDeath();
	}
}

public class PumpkinRAE : RulesAndEffects
{
	public PumpkinRAE(Plant plant) : base(plant) { }
	public override bool CanBePlanted(HexTile tile)
	{
		HexTile[] neightbours = HexMapHolder.Current.GetNeighbours(tile);
		int numPumpkinsAround = 0;
		foreach (HexTile n in neightbours)
		{
			if (n.Owner == GameController.I.currentPlayer && n.Plant?.DisplayName == "Pumpkin")
			{
				numPumpkinsAround++;
			}
		}

		return numPumpkinsAround <= 1;
	}
}

public class WonderPlantRAE : RulesAndEffects
{
	public WonderPlantRAE(Plant plant) : base(plant) { }

	private int turnCounter = 0;
	public int turnsRemaining => GameController.I.wonderPlantVictoryTurns - turnCounter;
	public override void OnOwnerTurnStart()
	{
		turnCounter++;
		if (turnCounter >= GameController.I.wonderPlantVictoryTurns)
		{
			GameController.I.TriggerWonderVictory();
		}
	}

	public override bool CanBePlanted(HexTile tile)
	{
		bool isNextToRock = false;
		foreach (HexTile n in tile.Neighbours)
		{
			isNextToRock = n.TerrainType == TerrainType.Rock || isNextToRock;
		}

		return tile.Level >= 3 && !isNextToRock;
	}
}