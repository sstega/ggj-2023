using DefaultNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInputController : MonoBehaviour
{
	public static HexTile selectedTile;
	public static HexTile hoveredTile;

	void Update()
    {
	    HexTile oldHovered = hoveredTile;
	    hoveredTile = GetHoveredTile();
	    if (hoveredTile != oldHovered)
	    {
		    if (oldHovered != null) oldHovered.Hovered = false;
		    if (hoveredTile != null) hoveredTile.Hovered = true;
	    }
	    
		// do raycast on mouse up
		if (Input.GetMouseButton(0))
		{
			if (hoveredTile != null)
			{
				selectedTile = hoveredTile;
				GameController.I.playerSpawnsPlantAt(hoveredTile);
			}
		}

		// check if space is pressed
		if (Input.GetKeyDown(KeyCode.LeftAlt))
		{
			GameController.I.startNextPlayersTurn();
		}
	}

	private HexTile GetHoveredTile()
    {
	    if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject())
	    {
		    return null;
	    }
	    
	    // create ray from camera to world position
	    Vector3 mousePosition = Input.mousePosition;
	    Ray ray = Camera.main.ScreenPointToRay(mousePosition);
	    RaycastHit hit;
	    if (!Physics.Raycast(ray, out hit))
	    {
		    return null;
	    }
	    
	    return hit.collider.GetComponent<HexTile>();
    }
}
