using System;
using System.Collections.Generic;
using UnityEngine;
using Zen.Hexagons;

namespace DefaultNamespace
{
    public class HexMapHolder : MonoBehaviour
    {
        public static HexMapHolder Current;
        
        public float tileSize = 1f;
        public int gridSize = 5;
        
        private HexTile[][] grid;
        private HexLibrary hexLibrary;

        public List<HexTile> AllTiles
        {
            get
            {
                List<HexTile> result = new List<HexTile>();
                foreach (HexTile[] row in grid)
                {
                    foreach (HexTile tile in row)
                    {
                        result.Add(tile);
                    }
                }

                return result;
            }
        }


        private void OnEnable()
        {
            grid = new HexTile[gridSize][];
            for (int i = 0; i < gridSize; i++)
            {
                grid[i] = new HexTile[gridSize];
            }
            
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform tile = transform.GetChild(i);
                HexTile hexTile = tile.GetComponent<HexTile>();
                grid[hexTile.row][hexTile.col] = hexTile;
            }

            hexLibrary = new HexLibrary(HexType.FlatTopped, OffsetCoordinatesType.Odd, tileSize);

            Current = this;
        }
        
        public Vector3 GetCenterTilePosition()
        {
            Point2F pos2D = hexLibrary.FromOffsetCoordinatesToPixel(new HexOffsetCoordinates(gridSize / 2, gridSize / 2));
            return new Vector3(pos2D.X, 0, pos2D.Y);
        }

        public HexTile TileAt(int row, int col)
        {
            return grid[row][col];
        }

        public HexTile[] GetNeighbours(HexTile tile)
        {
            HexOffsetCoordinates offsetCoordinates = new HexOffsetCoordinates(tile.col, tile.row);
            HexOffsetCoordinatesSextuple offsetSextuple = hexLibrary.GetAllNeighbors(offsetCoordinates);
            HexOffsetCoordinates[] asArray = new[]
            {
                offsetSextuple.HexOffsetCoordinates0,
                offsetSextuple.HexOffsetCoordinates1,
                offsetSextuple.HexOffsetCoordinates2,
                offsetSextuple.HexOffsetCoordinates3,
                offsetSextuple.HexOffsetCoordinates4,
                offsetSextuple.HexOffsetCoordinates5,
            };

            List<HexTile> result = new List<HexTile>();
            foreach (var offset in asArray)
            {
                if (offset.Row < 0 || offset.Row >= gridSize || offset.Col < 0 || offset.Col >= gridSize)
                {
                    continue;
                }
                result.Add(TileAt(offset.Row, offset.Col));
            }

            return result.ToArray();
        }

        public void SetMap(HexTile[][] result, HexLibrary hexLib)
        {
            grid = result;
            hexLibrary = hexLib;
            gridSize = result.Length;
            tileSize = hexLib.Size;
        }
    }
}