using System;
using DefaultNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    private MeshRenderer mesh;
    private float OriginalLifespan;

    public string DisplayName;
    public int Cost;
    public int SeedsPerTurn;
    public int GrowthTime;
    public int PreferedTerrainLevel;
    public bool CanAttack;
    public float CombatBonusFactor;
    public float TerrainBonusFactor;
    public float FreeNeighbourBonusFactor;
    public Sprite DisplayImage;
    
    private RulesAndEffects raeI;
    public RulesAndEffects RulesAndEffects
    {
        get
        {
            if (this.raeI == null)
            {
                this.raeI = makeRAE();
            }
            return raeI;
        }
    }

	public List<TerrainType> AllowedTerrainTypes;

    [HideInInspector]
    public HexTile GrowingOn;

    private static readonly float maxYScale = 1.5f;

    private void Awake()
    {
	    mesh = GetComponent<MeshRenderer>();
	    OriginalLifespan = GrowthTime;
	}

    private void Start()
    {
	    RefreshModelScale();
    }

    public void Grow()
    {
        GrowthTime--;
        if (ShouldDie)
        {
	        RulesAndEffects.OnDeath();
        }
        else
        {
			RefreshModelScale();
        }
    }

    private void RefreshModelScale()
    {
	    float t = GrowthTime / OriginalLifespan;
	    float uniformScaleFactor = Mathf.Lerp(0.5f, 1f, t);
	    float terrainLevelScaleFactor = Mathf.Lerp(1, maxYScale, GrowingOn.Level / (float)HexTile.maxTerrainLevel);
	    transform.localScale = new Vector3(uniformScaleFactor, uniformScaleFactor * terrainLevelScaleFactor, uniformScaleFactor);
    }

    private int NeighboursBonus
    {
        get
        {
			int freeNeighbours = 0;
			foreach (HexTile n in GrowingOn.Neighbours)
			{
				if (n.Owner == this.GrowingOn.Owner && this.GrowingOn.Level > 0)
				{
					freeNeighbours++;
				}
			}

			float neighboursBonus = freeNeighbours * FreeNeighbourBonusFactor;
            return Mathf.RoundToInt(neighboursBonus);
		}
    }

    private int TerrainBonus
    {
        get
        {
            return Mathf.RoundToInt(this.GrowingOn.Level * TerrainBonusFactor);
		}
    }

    public int Harvest()
    {
        return SeedsPerTurn + NeighboursBonus + TerrainBonus;
    }

    public int CombatBonus
    {
        get
        {
            return Mathf.RoundToInt(SeedsPerTurn * CombatBonusFactor);
        }
    }

    public bool ShouldDie => GrowthTime <= 0;

    public void SetTint(Color color)
    {
	    mesh.material.color = color;
    }

	private RulesAndEffects makeRAE()
	{
		if (DisplayName == "Bush")
		{
			return new BushRAE(this);
		}
        if (DisplayName == "Flower")
        {
            return new FlowerRAE(this);
        }
		if (DisplayName == "Pumpkin")
		{
			return new PumpkinRAE(this);
		}
        if (DisplayName == "Mushroom")
        {
            return new MushroomRAE(this);
        }
        if (DisplayName == "Tree")
        {
	        return new WonderPlantRAE(this);
        }
		return new RulesAndEffects(this);
	}
}
