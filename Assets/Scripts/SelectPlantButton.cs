using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectPlantButton : MonoBehaviour
{
    [SerializeField] private Plant plant;
    [SerializeField] private TMPro.TextMeshProUGUI txtCost;
    [SerializeField] private Image internalButtonBG;
    [SerializeField] private Image plantCostBG;
    
    [SerializeField] private Color selectedColor;
    
    private Color normalBtnColor;
    private Color normalPlantCostBGColor;
    
    private void OnEnable()
    {
        txtCost.text = plant.Cost.ToString();
        normalBtnColor = internalButtonBG.color;
        internalButtonBG.sprite = plant.DisplayImage;
        normalPlantCostBGColor = plantCostBG.color;
    }

    public void OnInternalButtonClicked()
    {
        GameUIManager.I.SelectPlant(plant, this);
    }

    public void DisableSelectedMode()
    {
        internalButtonBG.color = normalBtnColor;
        plantCostBG.color = normalPlantCostBGColor;
    }
    
    public void EnableSelectedMode()
    {
        internalButtonBG.color = selectedColor;
        plantCostBG.color = selectedColor;
    }
}
