using System;
using UnityEngine;

namespace DefaultNamespace
{
    [ExecuteAlways]
    public class HexTile : MonoBehaviour
    {
        public static readonly int maxTerrainLevel = 5;
        
        [SerializeField] private Material dirtMaterial;
        [SerializeField] private Material rockMaterial;
        [SerializeField] private Material compostMaterial;

        [SerializeField]
        private TerrainType terrainType = TerrainType.Dirt;

        private Player owner;
        private int level = 0;
        private Color? highlightColor = null;

        [HideInInspector] public bool Hovered = false;

        private float terrainLevelColorModifier => Mathf.Lerp(0.3f, 1f, 1f / (level + 1));
        
        [HideInInspector]
        public int row;
        [HideInInspector]
        public int col;

        [HideInInspector] public Plant Plant { get; private set; }

        public Player Owner
        {
            get => owner;
            set { owner = value; RefreshView(); }
        }

        public int Level
        {
            get => level;
            set { level = Mathf.Clamp(value, 0, maxTerrainLevel); RefreshView(); }
        }
        
        public TerrainType TerrainType
        {
            get => terrainType;
            set
            {
                terrainType = value;
                RefreshView();
            }
        }

        public HexTile[] Neighbours => HexMapHolder.Current.GetNeighbours(this);

        public bool IsEmpty => Plant == null;

        [HideInInspector]
        private MeshRenderer mesh;
        private Outline outline;
        
        private void Awake()
        {
            mesh = GetComponent<MeshRenderer>();
            outline = GetComponent<Outline>();
            outline.enabled = false;
            
            RefreshView();
        }

        public void RefreshView()
        {
            switch (terrainType)
            {
                case TerrainType.Dirt:
                    mesh.material = dirtMaterial;
                    break;
                case TerrainType.Rock:
                    mesh.material = rockMaterial;
                    break;
                case TerrainType.Compost:
                    mesh.material = compostMaterial;
                    break;
            }

            if (!Application.isPlaying) // needed for level editor
            {
                return;
            }
            
            if (owner != null)
            {
                mesh.material.color = owner.Color;
                Plant?.SetTint(owner.Color);
            }

            mesh.material.color *= terrainLevelColorModifier;
        }

        private void Update()
        {
            if (Hovered)
            {
                outline.enabled = true;
                outline.OutlineColor = Color.yellow;
                return;
            }
            
            outline.enabled = highlightColor != null;
            if (highlightColor != null)
            {
                outline.OutlineColor = highlightColor.Value;
            }
        }

        public void SetPlant(Plant plant)
        {
			ClearPlant();
            Plant = Instantiate(plant, this.transform);
            Plant.GrowingOn = this;
			Plant.GrowthTime += Level;
			Plant.transform.localPosition = Vector3.zero;
            Plant.RulesAndEffects.OnPlant();
		}

        public void ClearPlant()
        {
            if (Plant != null)
            {
                Destroy(Plant.gameObject);
                Plant = null;
            }
            
            RefreshView();
        }

        public void SetOwner(Player newOwner)
        {
            owner?.OwnedTiles.Remove(this);
            owner = newOwner;
            
            RefreshView();
        }

        public void SetOutlineHighlight(Color newColor)
        {
            highlightColor = newColor;
        }

        public void ClearOutlineHighlight()
        {
            highlightColor = null;
        }
    }

    public enum TerrainType
    {
        Dirt = 0,
        Rock = 1,
        Compost = 2
    }
}