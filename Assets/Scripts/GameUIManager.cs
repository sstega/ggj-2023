using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    public static GameUIManager I { get; private set; }

    [SerializeField] private TMPro.TextMeshProUGUI txtCurrentPlayer;
    [SerializeField] private TMPro.TextMeshProUGUI txtSeeds;
    [SerializeField] private Image victoryPanel;
    [SerializeField] private TMPro.TextMeshProUGUI victoryPlayer;
    [SerializeField] private TMPro.TextMeshProUGUI victoryReason;
    [SerializeField] private TMPro.TextMeshProUGUI wonderVictoryPlayer1;
    [SerializeField] private TMPro.TextMeshProUGUI wonderVictoryPlayer2;


    private Color victoryPanelOriginalColor;
    private SelectPlantButton currentSelectedPlantButton;
    private Dictionary<Player, TextMeshProUGUI> playerToWonderTxt;

    private void Awake()
    {
        I = this;
        victoryPanelOriginalColor = victoryPanel.color;
        victoryPanel.gameObject.SetActive(false);
    }

    private void SetPlayer(Player player)
    {
        txtCurrentPlayer.text = $"Player {player.Name}'s Turn";
        txtSeeds.text = $"{player.Seeds}";
        txtCurrentPlayer.color = player.Color;
    }

    public void InitializeFirstTurn(Player firstPlayer, Player secondPlayer)
    {
        SetPlayer(firstPlayer);
        playerToWonderTxt = new Dictionary<Player, TextMeshProUGUI>();
        playerToWonderTxt[firstPlayer] = wonderVictoryPlayer1;
        playerToWonderTxt[secondPlayer] = wonderVictoryPlayer2;
    }
    
    public void SwitchTurn(Player oldPlayer, Player nextPlayer)
    {  
        SetPlayer(nextPlayer);
        int wonderTurnsRemaining = oldPlayer.WonderTurnsRemaining;
        if (wonderTurnsRemaining >= 0)
        {
            if (wonderTurnsRemaining == 0)
            {
                playerToWonderTxt[oldPlayer].text = $"Wonder victory for {oldPlayer.Name} NEXT TURN. Last chance to destroy his wonder, {nextPlayer.Name}!";
                playerToWonderTxt[oldPlayer].fontStyle = FontStyles.Bold;
            }
            else
            {
                playerToWonderTxt[oldPlayer].text = $"Wonder victory for {oldPlayer.Name} in {wonderTurnsRemaining} turns. Quick, destroy his wonder, {nextPlayer.Name}!";
                playerToWonderTxt[oldPlayer].fontStyle = FontStyles.Normal;
            }
            playerToWonderTxt[oldPlayer].gameObject.SetActive(true);
        }
        else
        {
            playerToWonderTxt[oldPlayer].gameObject.SetActive(false);
        }
    }

    public void SelectPlant(Plant plant, SelectPlantButton selectButton)
    {
        currentSelectedPlantButton?.DisableSelectedMode();
        GameController.I.selectPlant(plant);
        selectButton.EnableSelectedMode();
        currentSelectedPlantButton = selectButton;
    }

    public void OnPlantSpawned(HexTile tile)
    {
        SetPlayer(GameController.I.currentPlayer);
    }

    public void OnVictory(Player victor, string reason)
    {
        victoryPanel.color = victoryPanelOriginalColor * victor.Color;
        victoryPanel.gameObject.SetActive(true);
        victoryPlayer.text = $"{victor.Name} is victorious!";
        victoryReason.text = reason;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
