using DefaultNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
	public string Name;
	public int Seeds;
	public List<HexTile> OwnedTiles = new List<HexTile>();
	public HexTile HomeTile;
	public Color Color;

	public Player(string name, int seeds, HexTile homeTile, Color col)
	{
		Name = name;
		Seeds = seeds;
		HomeTile = homeTile;
		Color = col;
		homeTile.Owner = this;
	}

	public int WonderTurnsRemaining
	{
		get
		{
			int minTurns = int.MaxValue;
			foreach (var tile in OwnedTiles)
			{
				if (tile.Plant == null)
				{
					continue;
				}

				if (tile.Plant.RulesAndEffects != null && tile.Plant.RulesAndEffects is WonderPlantRAE rae)
				{
					minTurns = Mathf.Min(rae.turnsRemaining, minTurns);
				}
			}

			if (minTurns == int.MaxValue)
			{
				return -1;
			}

			return minTurns - 1;
		}
	}

	public List<HexTile> LegalTiles
	{
		get
		{
			List<HexTile> legalTiles = new List<HexTile>();

			foreach (HexTile neighbour in HomeTile.Neighbours)
			{
				if (neighbour.Owner == this || legalTiles.Contains(neighbour))
				{
					continue;
				}

				if (GameController.I.isValidMove(neighbour))
				{
					legalTiles.Add(neighbour);
				}
			}
			
			foreach (HexTile tile in OwnedTiles)
			{
				foreach (HexTile neighbour in tile.Neighbours)
				{
					if (neighbour.Owner == this || legalTiles.Contains(neighbour))
					{
						continue;
					}

					if (GameController.I.isValidMove(neighbour))
					{
						legalTiles.Add(neighbour);
					}
				}
			}
			return legalTiles;
		}
	}
}
