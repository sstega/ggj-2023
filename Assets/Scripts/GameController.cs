using System;
using DefaultNamespace;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	public static GameController I { get; private set; }
	
	public static int numberOfPlayers = 2;
	public static int currentPlayerIndex = 0;
	public static int currentTurn = 0;

	[Header("Game Settings")] 
	public int CompostSeedBonus;
	public int CompostTerrainLevelBonus;
	public Color playerOneColor;
	public Color playerTwoColor;
	public int startingSeeds; 
	public int wonderPlantVictoryTurns = 5;

	public Color legalEmptyTileColor;
	public Color legalCombatTileColor;
	

	public Player currentPlayer => players[currentPlayerIndex];
	public Player opponent => players[1 - currentPlayerIndex];
	private List<Player> players;

	[HideInInspector]
	public Plant selectedPlant;

	
	private void Awake()
	{
		SceneManager.LoadScene("Map3", LoadSceneMode.Additive);
		I = this;
	}
	
	void Start()
	{
		int gridSize = HexMapHolder.Current.gridSize;

		HexTile p1HomeTile = HexMapHolder.Current.TileAt(0, 0);
		HexTile p2HomeTile = HexMapHolder.Current.TileAt(gridSize - 1, gridSize - 1);

		players = new List<Player> { new Player("One", startingSeeds, p1HomeTile, playerOneColor), new Player("Two", startingSeeds, p2HomeTile, playerTwoColor) };
		p1HomeTile.Owner = players[0];
		p2HomeTile.Owner = players[1];

		HighlightLegalTiles();

		GameUIManager.I.InitializeFirstTurn(currentPlayer, opponent);
	}

	public void startNextPlayersTurn()
    {
	    Player oldPlayer = currentPlayer;
		currentPlayerIndex = (currentPlayerIndex + 1) % numberOfPlayers;
        if (currentPlayerIndex == 0)
        {
			currentTurn++;
		}

        foreach (var tile in currentPlayer.OwnedTiles)
        {
	        tile.Plant?.RulesAndEffects.OnOwnerTurnStart();
        }
        
        HarvestSeeds();
		PlantsGrow();
		
		GameUIManager.I.SwitchTurn(oldPlayer, currentPlayer);
		HighlightLegalTiles();
    }

	private void PlantsGrow()
	{
		List<HexTile> shouldRemoveOwner = new List<HexTile>();
		
		foreach (HexTile tile in currentPlayer.OwnedTiles)
		{
			tile.Plant?.Grow();
			if (tile.Plant == null)
			{
				shouldRemoveOwner.Add(tile);
			}
		}

		foreach (HexTile tile in shouldRemoveOwner)
		{
			tile.SetOwner(null);
		}
	}

	private void HarvestSeeds()
	{
		int seedsHarvested = 0;
		foreach (HexTile tile in currentPlayer.OwnedTiles)
		{
			if (tile.Plant != null)
			{
				seedsHarvested += tile.Plant.Harvest();
			}
		}
		currentPlayer.Seeds += seedsHarvested;
	}

	public void selectPlant(Plant plant)
	{
		selectedPlant = plant;
		HighlightLegalTiles();
	}

	public void playerSpawnsPlantAt(HexTile tile)
	{
		if (!isValidMove(tile) || selectedPlant == null)
		{
			return;
		}

		if (selectedPlant.CanAttack && tile.Owner != null && tile.Owner == opponent)
		{
			bool currentPlayerWon = ResolveCombatOn(tile);
			if (currentPlayerWon)
			{
				currentPlayer.Seeds -= selectedPlant.Cost;
				tile.SetPlant(selectedPlant);
				giveTileOwnersip(tile);
			}
		} else if (selectedPlant.AllowedTerrainTypes.Contains(tile.TerrainType) && tile.Owner == null)
		{
			currentPlayer.Seeds -= selectedPlant.Cost;
			tile.SetPlant(selectedPlant);
			giveTileOwnersip(tile);
		}

		GameUIManager.I.OnPlantSpawned(tile);
		HighlightLegalTiles();
	}

	public bool isValidMove(HexTile tile)
	{
		HexTile[] neighbours = HexMapHolder.Current.GetNeighbours(tile);

		bool hasOwnedNeighbour = false;
		foreach (HexTile neighbour in neighbours)
		{
			if (neighbour.Owner == currentPlayer && neighbour.Plant?.DisplayName != "Mushroom")
			{
				hasOwnedNeighbour = true;
				break;
			}
		}

		bool isNotOwnedByCurrentPlayer = tile.Owner != currentPlayer;
		bool haveEnoughSeeds = currentPlayer.Seeds >= selectedPlant.Cost;
		bool costAndRAECheck = hasOwnedNeighbour && isNotOwnedByCurrentPlayer && haveEnoughSeeds && selectedPlant.RulesAndEffects.CanBePlanted(tile);
		bool terrainTypeCheck = selectedPlant.AllowedTerrainTypes.Contains(tile.TerrainType);
		return costAndRAECheck && terrainTypeCheck;
	}

	private bool ResolveCombatOn(HexTile tile)
	{
		Player defender = tile.Owner;
		HexTile[] neighbours = HexMapHolder.Current.GetNeighbours(tile);

		List<HexTile> defendingForces = new List<HexTile>();
		List<HexTile> attackingForces = new List<HexTile>();

		foreach (HexTile t in neighbours)
		{
			if (t.Owner == defender)
			{
				defendingForces.Add(t);
			} else if (t.Owner == currentPlayer)
			{
				attackingForces.Add(t);
			}
		}

		float defendingScore = 0;
		float attackingScore = 0;

		foreach (HexTile d in defendingForces) 
		{
			defendingScore += d.Plant != null ? d.Level + d.Plant.CombatBonus : 0;
		}
		foreach (HexTile a in attackingForces)
		{
			attackingScore += a.Plant != null ? a.Plant.CombatBonus : 0;
		}

		attackingScore += selectedPlant.CombatBonus;

		bool attackerWon = attackingScore > defendingScore;

		print($"Combat: {currentPlayer.Name} {attackingScore} strenght from {attackingForces.Count} plants vs {defender.Name} {defendingScore} strength from {defendingForces.Count} plants -> {currentPlayer.Name} {(attackerWon ? "won" : "lost")}");

		HighlightLegalTiles();
		return attackerWon;
	}

	public void giveTileOwnersip(HexTile tile)
	{
		if (tile.Owner != null)
		{
			tile.Owner.OwnedTiles.Remove(tile);
		}
		
		currentPlayer.OwnedTiles.Add(tile);
		tile.Owner = currentPlayer;

		if (tile == opponent.HomeTile)
		{
			DoVictory(currentPlayer, $"Military victory: {opponent.Name}'s home tile was taken");
		}

		HighlightLegalTiles();
	}

	private void DoVictory(Player victor, string reason)
	{
		GameUIManager.I.OnVictory(victor, reason);
		Destroy(gameObject);
	}

	public void ApplyCompostBonus(HexTile tile)
	{
		tile.Level = CompostTerrainLevelBonus;
		currentPlayer.Seeds += CompostSeedBonus;
	}

	public void TriggerWonderVictory()
	{
		DoVictory(currentPlayer, $"Wonder victory: {currentPlayer.Name}'s wonder plant survived for {wonderPlantVictoryTurns} turns");
	}

	public void HighlightLegalTiles()
	{
		foreach (HexTile tile in HexMapHolder.Current.AllTiles)
		{
			tile.ClearOutlineHighlight();
		}

		if (selectedPlant == null)
		{
			return;
		}
		
		foreach (HexTile tile in currentPlayer.LegalTiles)
		{
			if (tile.Owner == opponent)
			{
				tile.SetOutlineHighlight(legalCombatTileColor);
			}
			else
			{
				tile.SetOutlineHighlight(legalEmptyTileColor);
			}
		}
	}
}
